//
//  InitializerBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 27/4/21.
//

import SwiftUI

struct InitializerBootCamp: View {
    let backgroundColor: Color
    let count: Int
    let title: String
    
    init(background: Color, count: Int, title: String) {
        self.backgroundColor = background
        self.count = count
        self.title = title
    }
    
    var body: some View {
        VStack {
            Text("\(count)")
                .font(.largeTitle)
                .foregroundColor(.white)
                .underline()
            
            Text(title)
                .font(.largeTitle)
                .foregroundColor(.white)
        }
        .frame(width: 150, height: 150)
        .background(Color.red)
        .cornerRadius(10)
    }
}

struct InitializerBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        InitializerBootCamp(background: .red, count: 55, title: "Apples")
    }
}
