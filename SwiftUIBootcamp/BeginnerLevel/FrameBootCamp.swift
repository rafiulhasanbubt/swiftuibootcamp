//
//  FrameBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 27/4/21.
//

import SwiftUI

struct FrameBootCamp: View {
    var body: some View {
        Text("Hello, World!")
            .background(Color.red)
            
            .frame(height: 100)
            .background(Color.orange)
            
            .frame(width: 150)
            .background(Color.purple)
            
            .frame(maxWidth: .infinity)
            .background(Color.pink)
            
            .frame(height: 400)
            .background(Color.green)
            
            .frame(maxHeight: .infinity)
            .background(Color.yellow)
    }
}

struct FrameBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        FrameBootCamp()
    }
}
