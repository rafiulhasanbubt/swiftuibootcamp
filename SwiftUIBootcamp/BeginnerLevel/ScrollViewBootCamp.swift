//
//  ScrollViewBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 27/4/21.
//

import SwiftUI

struct ScrollViewBootCamp: View {
    var body: some View {
        ScrollView(showsIndicators: true) {
            VStack {
                ForEach(0 ..< 10) { item in
                    ScrollView(.horizontal, showsIndicators: true, content: {
                        HStack {
                            ForEach(0 ..< 50) { item in
                                RoundedRectangle(cornerRadius:10)
                                    .fill(Color.white)
                                    .frame(width: 200, height: 150)
                                    .shadow(radius: 4)
                                    .padding()
                            }
                        }
                    })
                    
                }
            }
        }
    }
}

struct ScrollViewBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        ScrollViewBootCamp()
    }
}
