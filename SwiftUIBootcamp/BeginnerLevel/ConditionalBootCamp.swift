//
//  ConditionalBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 28/4/21.
//

import SwiftUI

struct ConditionalBootCamp: View {
    @State var showCircle: Bool = false
    @State var showRectangle: Bool = false
    
    var body: some View {
        VStack {
            Spacer()
            
            Button("Circle Button") {
                showCircle.toggle()
            }
            
            Button("Rectangle Button") {
                showRectangle.toggle()
            }
            
            if showCircle {
                Circle()
                    .frame(width: 100, height: 100)
            } else if showRectangle {
                Rectangle()
                    .frame(width: 100, height: 100)
            } else {
                RoundedRectangle(cornerRadius: 25.0)
                    .frame(width: 200, height: 100)
            }
            
            Spacer()
        }
    }
}

struct ConditionalBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        ConditionalBootCamp()
    }
}
