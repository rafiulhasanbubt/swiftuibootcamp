//
//  TabViewBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 29/4/21.
//

import SwiftUI

struct TabViewBootCamp: View {
    @State var selectedTab: Int = 0
    let icons: [String] = ["heart.fill", "globe", "bell", "person.fill", "house.fill", "person.crop.rectangle.fill"]
    
    var body: some View {
        VStack {
            //Page Tabview Style
            VStack {
                TabView {
                    ForEach(icons, id: \.self) { icon in
                        Image(systemName: icon)
                            .resizable()
                            .scaledToFit()
                            .padding(20)
                    }
                }
                .background(
                    RadialGradient(gradient: Gradient(colors: [Color.red, Color.blue]), center: .center, startRadius: 50, endRadius: 400)
                )
                .frame(height: 300)
                .tabViewStyle(PageTabViewStyle())
            }
            
            //tabview Style applyed
            TabView {
                Text("Home")
                    .tabItem {
                        Image(systemName: "house.fill")
                        Text("Home")
                    }

                Text("Browse")
                    .tabItem {
                        Image(systemName: "globe")
                        Text("Browse")
                    }

                Text("Profile")
                    .tabItem {
                        Image(systemName: "person.fill")
                        Text("Profile")
                    }

                Text("Notification")
                    .tabItem {
                        Image(systemName: "bell.fill")
                        Text("Notification")
                    }

                Text("Favourite")
                    .tabItem {
                        Image(systemName: "heart.fill")
                        Text("favourite")
                    }
            }
            .accentColor(.red)
            
        }
        
    }
}

struct TabViewBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        TabViewBootCamp()
    }
}
