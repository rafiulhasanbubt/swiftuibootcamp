//
//  SliderBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 29/4/21.
//

import SwiftUI

struct SliderBootCamp: View {
    @State var sliderValue: Double = 5
    @State var color: Color = .red
    
    var body: some View {
        VStack(alignment: .center, spacing: 20.0) {
            Text("Rating:")
            Text(String(format: "%.2f", sliderValue))
                .foregroundColor(color)
                .font(.title)
            
            //Slider(value: $sliderValue)
            //Slider(value: $sliderValue, in: 1...10)
            //Slider(value: $sliderValue, in: 1...5, step: 0.25)
            Slider(
                value: $sliderValue,
                in: 1...5,
                step: 0.25,
                onEditingChanged: { (_) in
                    color = .green
                },
                minimumValueLabel: Text("1"),
                maximumValueLabel: Text("5"),
                label:  {
                    Text("Title")
                }
            )
        }
        .padding()
    }
}

struct SliderBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        SliderBootCamp()
    }
}
