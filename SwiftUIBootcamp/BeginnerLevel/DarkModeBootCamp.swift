//
//  DarkModeBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 29/4/21.
//

import SwiftUI

struct DarkModeBootCamp: View {
    @Environment(\.colorScheme) var colorScheme
    
    var body: some View {
        NavigationView {
            ScrollView {
                VStack(spacing: 20.0) {
                    Text("This color is Primary")
                        .foregroundColor(.primary)
                    Text("This color is Secondary")
                        .foregroundColor(.secondary)
                    Text("This color is Black")
                        .foregroundColor(.black)
                    Text("This color is White")
                        .foregroundColor(.white)
                    Text("This color is red")
                        .foregroundColor(.red)
                    Text("This color is globally adaptive")
                        .foregroundColor(.accentColor)
                    Text("This color is Locally Adaptive")
                        .foregroundColor(colorScheme == .light ? .green : .yellow)
                }
            }
        }
    }
}

struct DarkModeBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        DarkModeBootCamp()
    }
}
