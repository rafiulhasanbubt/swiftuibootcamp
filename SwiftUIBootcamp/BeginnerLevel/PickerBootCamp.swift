//
//  PickerBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 29/4/21.
//

import SwiftUI

struct PickerBootCamp: View {
    @State var selection: String = "Most Recent"
    let filterOptions: [String] = ["Most Recent", "Most Popular", "Most Liked"]
    
    init() {
        UISegmentedControl.appearance().selectedSegmentTintColor = UIColor.red
        let attributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.white]
        UISegmentedControl.appearance().setTitleTextAttributes(attributes, for: .selected)
    }
    
    var body: some View {
        VStack {
            VStack {
                Picker(selection: $selection, label: Text("Picker"), content: {
                    ForEach(filterOptions.indices) { index in
                        Text(filterOptions[index])
                            .tag(filterOptions[index])
                    }
                })
                .pickerStyle(SegmentedPickerStyle())
            }
            .padding()
            
            VStack {
                Picker(
                    selection: $selection,
                    label:
                        HStack{
                            Text("Filter")
                            Text(selection)
                        }
                        .font(.headline)
                        .foregroundColor(.white)
                        .padding()
                        .padding(.horizontal)
                        .background(Color.blue)
                        .cornerRadius(10)
                        .shadow(color: Color.blue.opacity(0.3), radius: 10, x: 0, y: 10)
                    
                    , content: {
                        ForEach(filterOptions, id:\.self) { option in
                            Text(option)
                                .tag(option)
                        }
                    })
                    .pickerStyle(MenuPickerStyle())
            }
            .padding()
            
            VStack {
                HStack{
                    Text("Age: ")
                    Text(selection)
                }
                
                Picker(selection: $selection, label: Text("Picker"), content: {
                    ForEach(18..<100) { number in
                        Text("\(number)")
                            .tag("\(number)")
                    }
                })
            }
            .padding()
        }
    }
}

struct PickerBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        PickerBootCamp()
    }
}
