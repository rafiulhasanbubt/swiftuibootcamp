//
//  ExtractedSubviewsBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 27/4/21.
//

import SwiftUI

struct ExtractedSubviewsBootCamp: View {
    var body: some View {
        ZStack {
            Color(#colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)).edgesIgnoringSafeArea(.all)
            CustomView()
        }
    }
}

struct ExtractedSubviewsBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        ExtractedSubviewsBootCamp()
    }
}

struct CustomView: View {
    var body: some View {
        VStack {
            Text("1")
            Text("Apples")
        }
        .padding()
        .background(Color.red)
        .cornerRadius(10)
        .foregroundColor(.white)
    }
}
