//
//  PopoverBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 28/4/21.
//

//sheets
//animations
//transitions

import SwiftUI

struct PopoverBootCamp: View {
    @State var showNewScreen: Bool = false
    
    var body: some View {
        ZStack {
            Color.orange.edgesIgnoringSafeArea(.all)
            
            VStack{
                Button(action: {
                    showNewScreen.toggle()
                }, label: {
                    Text("Button")
                        .font(.largeTitle)
                })
                
                Spacer()
            }
            // Method -> 01
            //.sheet(isPresented: $showNewScreen, content: { NewScreen() })
            
            //Method -> 02
//            ZStack {
//                if showNewScreen {
//                    NewScreen(showNewScreen: $showNewScreen)
//                        .padding(.top, 100)
//                        .transition(.move(edge: .bottom))
//                        .animation(.spring())
//                }
//            }.zIndex(2.0)
            
            //Method -> 03
            NewScreen(showNewScreen: $showNewScreen)
                //.padding(.top, 100)
                .offset(y: showNewScreen ? 0 : UIScreen.main.bounds.height)
                .animation(.spring())
        }
    }
}

struct NewScreen: View {
    @Environment (\.presentationMode) var presentationMode
    @Binding var showNewScreen: Bool
    
    var body: some View {
        ZStack (alignment: .topLeading) {
            Color.purple.edgesIgnoringSafeArea(.all)
            
            Button(action: {
                //presentationMode.wrappedValue.dismiss()
                showNewScreen.toggle()
            }, label: {
                Image(systemName: "xmark")
                    .foregroundColor(.white)
                    .font(.largeTitle)
                    .padding(20)
            })
        }
    }
}

struct PopoverBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        PopoverBootCamp()
    }
}
