//
//  IconsBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 27/4/21.
//

import SwiftUI

struct IconsBootCamp: View {
    var body: some View {
        Image(systemName: "heart.fill")
            //.font(.title)
            //.font(.system(size: 100))
            
            .resizable()
            //.aspectRatio(contentMode: .fill)
            //.scaledToFit()
            .scaledToFill()
            .foregroundColor(.red)
            .frame(width: 200, height: 200)
            .clipped()
    }
}

struct IconsBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        IconsBootCamp()
    }
}
