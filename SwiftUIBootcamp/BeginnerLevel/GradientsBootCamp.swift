//
//  GradientsBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 26/4/21.
//

import SwiftUI

struct GradientsBootCamp: View {
    var body: some View {
        RoundedRectangle(cornerRadius: 25.0)
            .fill(
                //LinearGradient(gradient: Gradient(colors: [Color.red, Color.blue]), startPoint: .topLeading, endPoint: .bottom)
                //RadialGradient(gradient: Gradient(colors: [Color.red, Color.blue]), center: .leading, startRadius: 5, endRadius: 400)
                AngularGradient(gradient: Gradient(colors: [Color.red, Color.blue]), center: .center, startAngle: .zero, endAngle: .degrees(180))
            )
            .frame(width: 300, height: 200)
    }
}

struct GradientsBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        GradientsBootCamp()
    }
}
