//
//  SpacerBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 27/4/21.
//

import SwiftUI

struct SpacerBootCamp: View {
    var body: some View {
        VStack {
            HStack(spacing: 0) {
                Image(systemName: "xmark")
                Image(systemName: "gear")
            }
            .font(.title)
            .background(Color.yellow)
            
            
            HStack {
                Spacer()
                    .frame(height: 10)
                    .background(Color.yellow)
                
                Rectangle()
                    .frame(width: 50, height: 50)
                
                Rectangle()
                    .fill(Color.red)
                    .frame(width: 50, height: 50)
                
                Rectangle()
                    .fill(Color.green)
                    .frame(width: 50, height: 50)
            }
            .background(Color.blue)
        }
    }
}

struct SpacerBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        SpacerBootCamp()
    }
}
