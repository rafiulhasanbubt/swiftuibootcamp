//
//  BackgroundOverlayBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 27/4/21.
//

import SwiftUI

struct BackgroundOverlayBootCamp: View {
    var body: some View {
        VStack {
            Spacer()
            
            VStack{
                Image(systemName: "heart.fill")
                    .font(.system(size: 40))
                    .foregroundColor(.white)
                    .background(
                        Circle()
                            .fill(
                                LinearGradient(gradient: Gradient(colors: [Color.red, Color.blue]), startPoint: .topLeading, endPoint: .bottomTrailing)
                            )
                            .frame(width: 100, height: 100)
                            .shadow(color: .red, radius: 10, x: 0.0, y: 10)
                            .overlay(
                                Circle()
                                    .fill(Color.blue)
                                    .frame(width: 35, height: 35)
                                    .overlay(
                                        Text("5")
                                            .font(.headline)
                                            .foregroundColor(.white)
                                    )
                                    .shadow(color: .red, radius: 10, x: 0.0, y: 10)
                                , alignment: .bottomTrailing
                            )
                    )
            }
            
            Spacer()
            
            VStack{
                Rectangle()
                    .frame(width: 100, height: 100)
                    .overlay(
                        Rectangle()
                            .fill(Color.blue)
                            .frame(width: 50, height: 50)
                        , alignment: .topLeading
                    )
                    .background(
                        Rectangle()
                            .fill(Color.red)
                            .frame(width: 150, height: 150)
                        , alignment: .bottomTrailing
                    )
            }
            
            Spacer()
            
            VStack{
                Circle()
                    .fill(Color.pink)
                    .frame(width: 100, height: 100)
                    .overlay(
                        Text("12")
                            .font(.largeTitle)
                            .foregroundColor(.white)
                    )
                    .background(
                        Circle()
                            .fill(Color.purple)
                            .frame(width: 110, height: 110)
                    )
            }
            
            Spacer()
        }
    }
}

struct BackgroundOverlayBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        BackgroundOverlayBootCamp()
    }
}
