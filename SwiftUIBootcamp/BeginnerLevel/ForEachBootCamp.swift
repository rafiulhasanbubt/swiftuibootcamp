//
//  ForEachBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 27/4/21.
//

import SwiftUI

struct ForEachBootCamp: View {
    let data: [String] = ["Hi", "Hello", "Hey everyOne"]
    let myString: String = "Hello"
    
    var body: some View {
        VStack {
            VStack {
                ForEach(data.indices) { index  in
                    Text("\(data[index]): \(index)")
                }
            }
            
            VStack {
                ForEach(0 ..< 10) { item in
                    
                    HStack {
                        Circle()
                            .frame(width: 20, height: 20)
                        
                        Text("Hello, World! \(item)")
                    }
                }
            }
        }
    }
}

struct ForEachBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        ForEachBootCamp()
    }
}
