//
//  ShapesBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 26/4/21.
//

import SwiftUI

struct ShapesBootCamp: View {
    var body: some View {
        //Circle()
        //Ellipse()
        //Capsule(style: .circular)
        //Rectangle()
        RoundedRectangle(cornerRadius: 30, style: .circular)
            //.fill(Color.red)
            //.foregroundColor(.blue)
            //.stroke()
            //.stroke(Color.red)
            //.stroke(Color.red, lineWidth: 4.0)
            //.stroke(Color.orange, style: StrokeStyle(lineWidth: 20, lineCap: .square, dash: [30]))
            //.trim(from: 0.3, to: 1.0)
            .frame(width: 200, height: 100, alignment: .center)
    }
}

struct ShapesBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        ShapesBootCamp()
    }
}
