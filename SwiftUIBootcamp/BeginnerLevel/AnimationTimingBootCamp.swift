//
//  AnimationTimingBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 28/4/21.
//

import SwiftUI

struct AnimationTimingBootCamp: View {
    @State var isAnimated: Bool = false
    
    var body: some View {
        VStack {
            Spacer()
            
            Button("Button") {
                isAnimated.toggle()
            }
            .padding()
            .padding(.horizontal)
            .foregroundColor(.white)
            .background(Color.blue)
            .cornerRadius(15)
            
            RoundedRectangle(cornerRadius:20)
                .frame(width: isAnimated ? 350 : 50, height: 100)
                .animation(.spring(response: 0.5, dampingFraction: 0.7, blendDuration: 1.0))
                //.animation(.spring())
                //.animation(Animation.linear)
            
            RoundedRectangle(cornerRadius:20)
                .frame(width: isAnimated ? 350 : 50, height: 100)
                .animation(Animation.easeIn)
            
            RoundedRectangle(cornerRadius:20)
                .frame(width: isAnimated ? 350 : 50, height: 100)
                .animation(Animation.easeInOut)
            
            RoundedRectangle(cornerRadius:20)
                .frame(width: isAnimated ? 350 : 50, height: 100)
                .animation(Animation.easeOut)
            
            Spacer()
        }
    }
}

struct AnimationTimingBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        AnimationTimingBootCamp()
    }
}
