//
//  ColorsBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 26/4/21.
//

import SwiftUI

struct ColorsBootCamp: View {
    var body: some View {
        RoundedRectangle(cornerRadius: 50)
            .fill(
                //Color.primary
                //Color(#colorLiteral(red: 0.04233003408, green: 0.353949368, blue: 0.680811882, alpha: 1))
                //Color(UIColor.systemPurple)
                Color("CustomColor")
            )
            .frame(width: 300, height: 200)
            //.shadow(radius: 10 )
            .shadow(color: Color.red.opacity(0.3), radius: 16, x: 10, y: 20)
    }
}

struct ColorsBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        ColorsBootCamp()
            
    }
}
