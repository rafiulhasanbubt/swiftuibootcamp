//
//  TextEditorBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 29/4/21.
//

import SwiftUI

struct TextEditorBootCamp: View {
    @State var textEditor: String = " This is the starting text"
    
    var body: some View {
        NavigationView {
            VStack {
                TextEditor(text: $textEditor)
                
                Button(action: {
                    
                }, label: {
                    Text("save".uppercased())
                        .font(.headline)
                        .foregroundColor(.white)
                        .padding()
                        .frame(maxWidth: .infinity)
                        .background(Color.blue)
                        .cornerRadius(10)
                })
                
            }
            .padding()
            .background(Color.gray.opacity(0.2))
            .navigationTitle("Texteditor Bootcamp")
        }
    }
}

struct TextEditorBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        TextEditorBootCamp()
    }
}
