//
//  ToggleBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 29/4/21.
//

import SwiftUI

struct ToggleBootCamp: View {
    @State var toggleIsOn: Bool = false
    
    var body: some View {
        VStack {
            HStack {
                Text("Status:")
                Text(toggleIsOn ? "Online" : "Offline")
            }.font(.title)
            
            Toggle(
                isOn: $toggleIsOn, label: {
                    Text("Change status")
                }
            ).toggleStyle(SwitchToggleStyle(tint: Color.green))
            
            Spacer()
        }
        .padding(.horizontal)
    }
}

struct ToggleBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        ToggleBootCamp()
    }
}
