//
//  StepperBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 29/4/21.
//

import SwiftUI

struct StepperBootCamp: View {
    @State var stepperValue: Int = 10
    @State var widthIncrement: CGFloat = 0
    
    var body: some View {
        VStack {
            VStack {
                Stepper("Stepper: \(stepperValue)", value: $stepperValue)
            }.padding()
            
            RoundedRectangle(cornerRadius: 25)
                .frame(width: 100 + widthIncrement, height: 100)
            
            VStack {
                Stepper("Stepper: ") {
                    incrementWidth(amount: 100)
                } onDecrement: {
                    incrementWidth(amount: -100)
                }
            }
            .padding()
        }
    }
    
    func incrementWidth(amount: CGFloat) {
        withAnimation(.easeInOut) {
            widthIncrement += amount
        }
    }
}

struct StepperBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        StepperBootCamp()
    }
}
