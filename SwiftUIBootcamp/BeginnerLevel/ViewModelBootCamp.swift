//
//  ViewModelBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 30/4/21.
//

import SwiftUI

//Model
struct FruitModel: Identifiable {
    let id: String = UUID().uuidString
    let name: String
    let count: Int
}

//ViewModel
class FruitViewModel: ObservableObject {
    @Published var fruitArray: [FruitModel] = []
    @Published var isLoading: Bool = false
    
    init() {
        getFruit()
    }
    
    func getFruit() {
        let fruit = FruitModel(name: "Apple", count: 18)
        let fruit1 = FruitModel(name: "Orange", count: 28)
        let fruit2 = FruitModel(name: "Banana", count: 40)
        let fruit3 = FruitModel(name: "Watermelon", count: 50)
        let fruit4 = FruitModel(name: "Guava", count: 36)
        
        isLoading = true
        DispatchQueue.main.asyncAfter(deadline: .now()+3) {
            self.fruitArray.append(fruit)
            self.fruitArray.append(fruit1)
            self.fruitArray.append(fruit2)
            self.fruitArray.append(fruit3)
            self.fruitArray.append(fruit4)
            self.isLoading = false
        }
    }
    
    func deleteFruit(index: IndexSet) {
        fruitArray.remove(atOffsets: index)
    }
}

//View
struct ViewModelBootCamp: View {
    //@State var fruitArray: [FruitModel] = ]
    //@StateObject -> use this on creation / init
    //@ObservedObject -> use this for subviews
    @StateObject var fruitViewModel: FruitViewModel = FruitViewModel()
    
    var body: some View {
        NavigationView {
            List {
                if fruitViewModel.isLoading {
                    ProgressView()
                        .foregroundColor(.green)
                } else {
                    ForEach(fruitViewModel.fruitArray) { fruit in
                        HStack{
                            Text("\(fruit.count)")
                            Text(fruit.name)
                        }
                    }
                    .onDelete(perform: fruitViewModel.deleteFruit)
                }
            }
            .listStyle(GroupedListStyle())
            .navigationTitle("Fruit List")
            .navigationBarItems(trailing: NavigationLink(
                                    destination: RandomScreen(fruitViewModel: fruitViewModel),
                                    label: {
                                        Image(systemName: "arrow.right").font(.title)
                                    }))
            
        }
    }
}

struct RandomScreen: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var fruitViewModel: FruitViewModel
    
    var body: some View {
        ZStack {
            Color.green.ignoresSafeArea()

            VStack {
                Spacer()
                
                VStack{
                    ForEach(fruitViewModel.fruitArray) {fruit in
                        Text(fruit.name)
                            .foregroundColor(.white)
                            .font(.headline)
                    }
                }
                Spacer()
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    Text("go back".uppercased())
                        .foregroundColor(.white)
                        .font(.largeTitle)
                        .fontWeight(.semibold)
                        .padding()
                        .padding(.horizontal)
                        .background(Color.blue)
                        .cornerRadius(20)
                })
                Spacer()
            }
        }
    }
}

struct ViewModelBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        ViewModelBootCamp()
    }
}
