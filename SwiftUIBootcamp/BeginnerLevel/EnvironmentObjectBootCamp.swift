//
//  EnvironmentObjectBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 30/4/21.
//

import SwiftUI

//ObservedObject
//StateObject
//EnvironmentObject

struct EnvironmentObjectBootCamp: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct EnvironmentObjectBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        EnvironmentObjectBootCamp()
    }
}
