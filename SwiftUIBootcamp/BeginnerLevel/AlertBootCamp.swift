//
//  AlertBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 28/4/21.
//

import SwiftUI

struct AlertBootCamp: View {
    @State var showAlert: Bool = false
    @State var backgroundColor: Color = Color.orange
    @State var alertType: MyType? = nil
    
    enum MyType {
        case error
        case success
    }
    
    var body: some View {
        ZStack {
            backgroundColor.edgesIgnoringSafeArea(.all)
            
            VStack {
                Button("Error Button") {
                    alertType = .error
                    showAlert.toggle()
                }
                
                Button("Success Button") {
                    alertType = .success
                    showAlert.toggle()
                }
            }
            .alert(isPresented: $showAlert, content: {
                getAlert()
            })
            
        }
    }
    
    func getAlert() -> Alert {
        switch alertType {
        case .error:
            return Alert(title: Text("There was an error!"))
        case .success:
            return Alert(title: Text("ActionName"), message: Text("Action description"), primaryButton: .default(Text("Action Type"), action: {
                backgroundColor = .green
            }), secondaryButton: .cancel())
            
        default:
            return Alert(title: Text("Error"))
        }
        
        //return Alert(title: Text("Alert"))
        //return Alert(title: Text("Title"), message: Text("Instructions"), primaryButton: .default(Text("Delete")), secondaryButton: .cancel())
//        return Alert(title: Text("ActionName"), message: Text("Action description"), primaryButton: .default(Text("Action Type"), action: {
//            backgroundColor = .green
//        }), secondaryButton: .cancel())
    }
}

struct AlertBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        AlertBootCamp()
    }
}
