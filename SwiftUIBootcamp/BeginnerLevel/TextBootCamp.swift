//
//  TextBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 26/4/21.
//

import SwiftUI

struct TextBootCamp: View {
    var body: some View {
        Text("SwiftUI BootCamp Tutorial".uppercased())
            //.font(.title)
            //.font(.system(size: 24, weight: .light, design: .serif))
            //.fontWeight(.heavy)
            //.bold()
            //.underline()
            //.italic()
            //.underline(true, color: Color.gray)
            //.strikethrough(true, color: Color.red)
            //.baselineOffset(10)
            //.kerning(10)
            //.multilineTextAlignment(.center)
            //.foregroundColor(.green)
            //.frame(width: 200, height: 100, alignment: .center)
            //.minimumScaleFactor(0.5)
    }
}

struct TextBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        TextBootCamp()
    }
}
