//
//  GridBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 27/4/21.
//

import SwiftUI

struct GridBootCamp: View {
    let column: [GridItem] = [
        GridItem(.flexible(), spacing: 6, alignment: nil),
        GridItem(.flexible(), spacing: 6, alignment: nil),
        GridItem(.flexible(), spacing: 6, alignment: nil)
    ]
    
    var body: some View {
        ScrollView{
            Rectangle()
                .fill(Color.orange)
                .frame(height: 300)
            
            LazyVGrid (columns: column,
                       alignment: .center,
                       pinnedViews: [.sectionHeaders],
                       content: {
                        Section(header: Text("Section 1")
                                    .foregroundColor(.white)
                                    .font(.title)
                                    .frame(maxWidth: .infinity)
                                    .background(Color.blue)
                                    .padding(.vertical, 8)
                        ) {
                            ForEach(0..<20) { index in
                                Rectangle()
                                    .fill(Color.white)
                                    .frame(height: 150)
                                    .shadow(radius: 4)
                            }
                        }
                        
                        Section(header: Text("Section 2")
                                    .foregroundColor(.white)
                                    .font(.title)
                                    .frame(maxWidth: .infinity)
                                    .background(Color.red)
                                    .padding(.vertical, 8)
                        ) {
                            ForEach(0..<20) { index in
                                Rectangle()
                                    .fill(Color.white)
                                    .frame(height: 150)
                                    .shadow(radius: 4)
                            }
                        }
                        
                        Section(header: Text("Section 3")
                                    .foregroundColor(.white)
                                    .font(.title)
                                    .frame(maxWidth: .infinity)
                                    .background(Color.green)
                                    .padding(.vertical, 8)
                        ) {
                            ForEach(0..<20) { index in
                                Rectangle()
                                    .fill(Color.white)
                                    .frame(height: 150)
                                    .shadow(radius: 4)
                            }
                        }
            })
        }
    }
}

struct GridBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        GridBootCamp()
    }
}
