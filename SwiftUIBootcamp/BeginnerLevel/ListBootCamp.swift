//
//  ListBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 28/4/21.
//

import SwiftUI

struct ListBootCamp: View {
    @State var fruits: [String] = ["Apple", "Orange", "Banana", "Peach"]
    @State var veggies: [String] = ["Tomato", "Potato", "Banana", "Carrot"]
    
    var body: some View {
        NavigationView {
            List {
                Section(header: HStack {
                    Text("Fruits")
                    Image(systemName: "flame.fill")
                }) {
                    ForEach(fruits, id: \.self) { fruit in
                        Text(fruit.capitalized)
                    }
                    .onDelete(perform: delete)
                    .onMove(perform: { indices, newOffset in
                        move(indices: indices, newOffset: newOffset)
                    })
                }
                
                Section(header: Text("Vegetables")) {
                    ForEach(veggies, id: \.self) { veggy in
                        Text(veggy.capitalized)
                    }
                }
            }
            
            .navigationTitle("Grocery List")
            .accentColor(Color.red)
            .navigationBarItems(leading: EditButton(), trailing: addButton)
        }
    }
    
    var addButton: some View {
        Button("Add", action: {
           add()
        })
    }
    
    func delete(inset: IndexSet) {
        fruits.remove(atOffsets: inset)
    }
    
    func move(indices: IndexSet, newOffset: Int) {
        fruits.move(fromOffsets: indices, toOffset: newOffset)
    }
    
    func add() {
        fruits.append("Coconut")
    }
}

struct ListBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        ListBootCamp()
    }
}
