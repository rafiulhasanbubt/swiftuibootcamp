//
//  ImageBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 27/4/21.
//

import SwiftUI

struct ImageBootCamp: View {
    var body: some View {
        Image("google")
            .resizable()
            .renderingMode(.template)
            .foregroundColor(.blue)
            //.aspectRatio(contentMode: .fill)
            //.scaledToFit()
            .scaledToFill()
            .frame(width: 300, height: 200)
            //.clipped()
            //.cornerRadius(50)
            //.clipShape(
                //Circle()
                //RoundedRectangle(cornerRadius: 30)
                //Rectangle()
                //Ellipse()
            //)
            
    }
}

struct ImageBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        ImageBootCamp()
    }
}
