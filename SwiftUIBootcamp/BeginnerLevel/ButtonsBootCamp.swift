//
//  ButtonsBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 27/4/21.
//

import SwiftUI

struct ButtonsBootCamp: View {
    @State var title: String = "This is my title"
    
    var body: some View {
        VStack (spacing: 30){
            Text(title)
            
            Button("Press me!") {
                self.title = "Button was pressed"
            }.accentColor(.red)
            
            Button(action: {
                self.title = "Button 2 was pressed"
            }) {
                Text("save".uppercased())
                    .font(.headline)
                    .fontWeight(.semibold)
                    .foregroundColor(.white)
                    .padding()
                    .padding(.horizontal, 20)
                    .background(
                        Color.blue
                            .cornerRadius(10)
                            .shadow(radius: 10)
                    )
            }
            
            Button(action: {
                self.title = "Button 3 was pressed"
            }) {
                Circle()
                    .fill(Color.white)
                    .frame(width: 75, height: 75)
                    .shadow(radius: 10)
                    .overlay(
                        Image(systemName: "heart.fill")
                            .font(.largeTitle)
                            .foregroundColor(.red)
                    )
            }
            
            Button(action: {
                self.title = "Button 4 was pressed"
            }) {
                Text("Finish".uppercased())
                    .font(.caption)
                    .bold()
                    .foregroundColor(.gray)
                    .padding()
                    .padding(.horizontal, 10)
                    .background(
                        Capsule()
                            .stroke(Color.gray, lineWidth: 2)
                    )
            }
        }
    }
}

struct ButtonsBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        ButtonsBootCamp()
    }
}
