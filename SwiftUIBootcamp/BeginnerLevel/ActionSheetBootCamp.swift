//
//  ActionSheetBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 28/4/21.
//

import SwiftUI

struct ActionSheetBootCamp: View {
    @State var showActionSheet: Bool = false
    @State var actionSheetOption: ActionSheetOptions = .isMypost
    
    enum ActionSheetOptions {
        case isMypost
        case isOtherpost
    }
    
    var body: some View {
        
        VStack {
            HStack {
                Circle()
                    .frame(width: 30, height: 30)
                
                Text("username")
                
                Spacer()
                
                Button(action: {
                    actionSheetOption = .isMypost
                    showActionSheet.toggle()
                }) {
                   Image(systemName: "ellipsis")
                    .foregroundColor(.blue)
                }
            }.padding(.horizontal)
            
            Rectangle()
                .aspectRatio(1.0, contentMode: .fit)
            
        }.actionSheet(isPresented: $showActionSheet, content: getActionSheet)
    }
    
    func getActionSheet() -> ActionSheet {
        let shareButton: ActionSheet.Button = .default(Text("Share")) {
            //
        }
        let reportButton: ActionSheet.Button = .destructive(Text("Report")) {
            //
        }
        let deleteButton: ActionSheet.Button = .destructive(Text("Delete")) {
            //
        }
        let cancelButton: ActionSheet.Button = .cancel()
        
        switch actionSheetOption {
        case .isOtherpost:
            return ActionSheet(title: Text("What would you like to do"), message: nil, buttons: [shareButton, reportButton,cancelButton])
        case .isMypost:
            return ActionSheet(title: Text("What would you like to do"), message: nil, buttons: [shareButton, reportButton,deleteButton,cancelButton])
        }
        
        //return ActionSheet(title: Text("This is the title!"))
        
//        let button1: ActionSheet.Button = .default(Text("Default"))
//        let button2: ActionSheet.Button = .destructive(Text("Destructive"))
//        let button3: ActionSheet.Button = .cancel(Text("Cancel"))
//        let button4: ActionSheet.Button = .cancel()
//
//        return ActionSheet(
//            title: Text("Title!"),
//            message: Text("Descriptions"),
//            buttons: [button1, button2, button3, button4]
//        )
    }
}

struct ActionSheetBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        ActionSheetBootCamp()
    }
}
