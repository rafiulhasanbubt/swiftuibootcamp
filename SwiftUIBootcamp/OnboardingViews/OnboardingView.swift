//
//  OnboardingView.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 30/4/21.
//

import SwiftUI

struct OnboardingView: View {
    //onboarding states
    //welcome screen
    //add name
    //add age
    //add gender
    
    @State var onboardingState: Int = 0
    @State var name: String = ""
    @State var age: Double = 50
    @State var gender: String = ""
    @State var alertTitle: String = ""
    @State var showAlert: Bool = false
    
    //AppStorage
    @AppStorage("name") var currentUserName: String?
    @AppStorage("age")  var currentUserAge: Int?
    @AppStorage("gender") var currentUserGender: String?
    @AppStorage("signned_in") var currentUserSignIn: Bool = false
    
    var transition: AnyTransition = .asymmetric(insertion: .move(edge: .trailing), removal: .move(edge: .leading))
    
    var body: some View {
        ZStack {
            //content
            ZStack {
                switch onboardingState {
                case 0:
                    welcomeSection
                        .transition(transition)
                case 1:
                    addNameSection
                        .transition(transition)
                case 2:
                    addAgeSection
                        .transition(transition)
                case 3:
                    addGenderSection
                        .transition(transition)
                default:
                    RoundedRectangle(cornerRadius: 25.0)
                        .foregroundColor(.green)
                }
            }
            
            //buttons
            VStack {
                Spacer()
                bottomButton
            }
            .padding()
        }
        .alert(isPresented: $showAlert, content: {
            return Alert(title: Text(alertTitle))
        })
    }
    
}

struct OnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingView().background(Color.purple)
    }
}


//MARK: Component
extension OnboardingView {
    
    var bottomButton: some View {
        Text(onboardingState == 0 ? "Sign up".uppercased() : onboardingState == 3 ? "finish".uppercased() : "next".uppercased())
            .font(.headline)
            .foregroundColor(.purple)
            .frame(height: 44)
            .frame(maxWidth: .infinity)
            .background(Color.white)
            .cornerRadius(10)
            .animation(nil)
            .onTapGesture {
                handleNextButtonPressed()
            }
    }
    
    
    var welcomeSection: some View {
        
        VStack (spacing:20) {
            Spacer()
            Image(systemName: "heart.text.square.fill")
                .resizable()
                .scaledToFit()
                .frame(width: 200, height: 200)
                .foregroundColor(.white)
            
            Text("Find your match.")
                .font(.largeTitle)
                .foregroundColor(.white)
                .fontWeight(.semibold)
                .overlay(
                    Capsule(style: .continuous)
                        .frame(height: 3)
                        .offset(y: 5)
                        .foregroundColor(.white)
                    , alignment: .bottom
                )
            
            Text("This is the #1 app for finding your match online! In this tutorial we are practicing using AppStorage and other SwiftUI techniques.")
                .fontWeight(.medium)
                .foregroundColor(.white)
            
            Spacer()
        }
        .padding()
    }
    
    
    var addNameSection: some View {
        VStack (spacing: 20) {
            Spacer()
            Text("What's your name?")
                .font(.largeTitle)
                .fontWeight(.semibold)
                .foregroundColor(.white)
            
            TextField("Your name here..", text: $name)
                .font(.headline)
                .frame(height: 44)
                .padding(.horizontal)
                .background(Color.white)
                .cornerRadius(10)
            
            Spacer()
        }
        .padding()
    }
    
    var addAgeSection: some View {
        VStack (spacing: 20) {
            Spacer()
            Text("What's your age?")
                .font(.largeTitle)
                .fontWeight(.semibold)
                .foregroundColor(.white)
            
            Text("\(String(format: "%.0f", age))")
                .font(.largeTitle)
                .fontWeight(.semibold)
                .foregroundColor(.white)
            
            Slider(value: $age, in: 18...100, step: 1)
                .accentColor(.white)
            
            Spacer()
        }
        .padding()
    }
    
    var addGenderSection: some View {
        VStack (spacing: 20) {
            Spacer()
            Text("What's your gender?")
                .font(.largeTitle)
                .fontWeight(.semibold)
                .foregroundColor(.white)
            
            Picker(selection: $gender,
                   label:
                    Text(gender.count > 1 ? gender : "Select a gender")
                    .font(.headline)
                    .foregroundColor(.purple)
                    .frame(height: 44)
                    .frame(maxWidth: .infinity)
                    .background(Color.white)
                    .cornerRadius(10)
                   , content: {
                    Text("Male").tag("Male")
                    Text("Female").tag("Female")
                    Text("Other").tag("Other")
                   })
                .pickerStyle(MenuPickerStyle())
            Spacer()
        }
        .padding()
    }
    
}

//MARK: Function
extension OnboardingView {
    
    func handleNextButtonPressed() {
        //check inputs
        switch onboardingState {
        case 1:
            guard name.count >= 3 else {
                showAlert(title: "Your name must be at least 3 characters longs!")
                return
            }
        case 3:
            guard gender.count >= 3 else {
                showAlert(title: "Please select a gender before moving forward!")
                return
            }
            
        default:
            break
        }
        
        //go to next section
        if onboardingState == 3 {
            //sign in
            signIn()
        } else {
            withAnimation(.spring()) {
                onboardingState += 1
            }
            
        }
    }
    
    func signIn() {
        currentUserName = name
        currentUserAge = Int(age)
        currentUserGender = gender
        withAnimation(.spring()) {
            currentUserSignIn = true
        }
    }
    
    func showAlert(title: String) {
        alertTitle = title
        showAlert.toggle()
    }
}
