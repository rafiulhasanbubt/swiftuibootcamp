//
//  TypealiasBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 5/5/21.
//

import SwiftUI

struct MovieModel {
    let title: String
    let director: String
    let count: Int
}

typealias TVModel = MovieModel

struct TypealiasBootCamp: View {
    //@State var item: MovieModel = MovieModel(title: "Movie", director: "Rafiul", count: 50)
    
    @State var item: TVModel = TVModel(title: "Avanger", director: "Rafiul", count: 90)
    var body: some View {
        VStack(spacing: 10) {
            Text(item.title)
            Text(item.director)
            Text("\(item.count)")
        }
        .frame(maxWidth: .infinity)
        .background(Color.white)
        .shadow(radius: 6)
        .padding()
        .padding(.horizontal)
    }
}

struct TypealiasBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        TypealiasBootCamp()
    }
}
