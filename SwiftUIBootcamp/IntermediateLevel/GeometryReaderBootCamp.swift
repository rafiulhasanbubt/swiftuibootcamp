//
//  GeometryReaderBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 1/5/21.
//

import SwiftUI

struct GeometryReaderBootCamp: View {
    
    var body: some View {
        
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                ForEach(0 ..< 20) { item in
                    GeometryReader{ geometry in
                        RoundedRectangle(cornerRadius: 20)
                            .rotation3DEffect(
                                Angle(degrees: getPercentage(geo: geometry) * 40),
                                axis: (x: 0.0, y: 1.0, z: 0.0))
                    }
                    .frame(width: 300, height: 250)
                }
            }
        }
        
//        GeometryReader { geometry in
//            HStack (spacing: 0) {
//                Rectangle()
//                    .fill(Color.red)
//                    .frame(width: UIScreen.main.bounds.width * 0.6)
//
//                Rectangle().fill(Color.blue)
//            }
//            .ignoresSafeArea()
//        }
    }
    
    func getPercentage(geo: GeometryProxy) -> Double {
        let maxDistance = UIScreen.main.bounds.width / 2
        let currentX = geo.frame(in: .global).midX
        return Double(1 - (currentX/maxDistance))
    }
}

struct GeometryReaderBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReaderBootCamp()
    }
}
