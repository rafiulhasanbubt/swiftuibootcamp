//
//  MaskBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 1/5/21.
//

import SwiftUI

struct MaskBootCamp: View {
    @State var rating: Int = 0
    
    var body: some View {
        ZStack {
            starsView
                .overlay(overlayView.mask(starsView))
        }
    }
    
    var overlayView: some View {
        GeometryReader { geometry in
            ZStack (alignment: .leading) {
                Rectangle()
                    .foregroundColor(.yellow)
                    .frame(width: CGFloat(rating) / 5 * geometry.size.width)
            }
        }.allowsTightening(false)
    }
    
    var starsView: some View {
        HStack {
            ForEach(1..<6) { index in
                Image(systemName: "star.fill")
                    .font(.largeTitle)
                    .foregroundColor(rating >= index ? Color.yellow : Color.gray)
                    .onTapGesture {
                        withAnimation(.easeInOut) {
                            rating = index
                        }
                    }
            }
        }
    }
}

struct MaskBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        MaskBootCamp()
    }
}
