//
//  EscapingBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 5/5/21.
//

import SwiftUI

class EscapingViewModel: ObservableObject {
    @Published var text: String = "Hello"
    
    func getData() {
        //let newData = downloadData()
        //text = newData
        
        //downloadData2 { (returnedData) in
        //    text = returnedData
        //}
        
        //downloadData3 { [weak self] (returnedData) in
        //    self?.text = returnedData
        //}
        
        downloadData4 { [weak self] (returnedResult) in
            self?.text = returnedResult.data
        }
    }
    
    func downloadData() -> String {
        return "New data!"
    }
    
    func downloadData2(completionHandler: (_ data: String) -> ()) {
        completionHandler("New data for 2!")
    }
    
    func downloadData3(completionHandler: @escaping (_ data: String) -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2 ) {
            completionHandler("New data for 3!")
        }
    }
    
    func downloadData4(completionHandler: @escaping (DownloadResult) -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2 ) {
            let result = DownloadResult(data: "New data for 4!")
            completionHandler(result)
        }
    }
    
    func downloadData5(completionHandler: @escaping DownloadCompletion) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2 ) {
            let result = DownloadResult(data: "New data for 4!")
            completionHandler(result)
        }
    }
    
    func doSomething(_ data: String) -> () {
        print(data)
    }
}

//Model
struct DownloadResult {
    let data: String
}

//typealias
typealias DownloadCompletion = (DownloadResult) -> ()

//View
struct EscapingBootCamp: View {
    @StateObject var vm = EscapingViewModel()
    
    var body: some View {
        Text(vm.text)
            .font(.largeTitle)
            .fontWeight(.semibold)
            .foregroundColor(.blue)
            .onTapGesture {
                vm.getData()
            }
    }
}

struct EscapingBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        EscapingBootCamp()
    }
}
