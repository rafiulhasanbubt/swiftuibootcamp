//
//  LocalNotificationBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 1/5/21.
//

import SwiftUI
import UserNotifications
import CoreLocation

class NotificationManager {
    static let instance = NotificationManager()
    
    func requestAuthorization() {
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        UNUserNotificationCenter.current().requestAuthorization(options: options) { (success, error) in
            if let error = error {
                print("Error: \(error)")
            } else {
                print("Success")
            }
        }
    }
    
    func scheduleNotification() {
        let content = UNMutableNotificationContent()
        content.title = "This is my first notification!"
        content.subtitle = "This was sooo easy!"
        content.sound = .default
        content.badge = 1
        
        //time
        //let triger = UNTimeIntervalNotificationTrigger(timeInterval: 5.0, repeats: false)
        
        //Calender
        //var dateComponents = DateComponents()
        //dateComponents.hour = 15
        //dateComponents.minute = 40
        //dataComponents.weekDay = 2
        //let triger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        
        //Location
        let coordinates = CLLocationCoordinate2D(latitude: 40, longitude: 50)
        let region = CLCircularRegion(center: coordinates, radius: 100, identifier: UUID().uuidString)
        region.notifyOnEntry = true
        region.notifyOnExit = true
        let triger = UNLocationNotificationTrigger(region: region, repeats: true)
        
        let request = UNNotificationRequest(
            identifier: UUID().uuidString,
            content: content,
            trigger: triger
        )
        
        UNUserNotificationCenter.current().add(request)
    }
    
    func cancelNotification() {
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
    }
}

struct LocalNotificationBootCamp: View {
    var body: some View {
        VStack (spacing: 20) {
            Button("Request permission") {
                NotificationManager.instance.requestAuthorization()
            }
            .font(.headline)
            .foregroundColor(.white)
            .padding()
            .padding(.horizontal)
            .background(Color.blue)
            .cornerRadius(10)
            
            Button("Schedule permission") {
                NotificationManager.instance.scheduleNotification()
            }
            .font(.headline)
            .foregroundColor(.white)
            .padding()
            .padding(.horizontal)
            .background(Color.blue)
            .cornerRadius(10)
        }
        .onAppear {
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
    }
}

struct LocalNotificationBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        LocalNotificationBootCamp()
    }
}
