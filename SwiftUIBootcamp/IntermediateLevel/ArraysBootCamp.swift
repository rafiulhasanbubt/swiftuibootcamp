//
//  ArraysBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 2/5/21.
//

import SwiftUI

struct ArraysUserModel: Identifiable {
    let id = UUID().uuidString
    let name: String
    let  points: Int
    let isVerified: Bool
}

class ArraysModificationViewMOdel: ObservableObject {
    @Published var dataArrays: [ArraysUserModel] = []
    @Published var filterArray: [ArraysUserModel] = []
    @Published var mappedArray: [String] = []
    
    init() {
        getUsers()
        updatefilteredArray()
    }
    
    func updatefilteredArray() {
        //sort
        /*
        filterArray = dataArrays.sorted {(user1, user2) -> Bool in
            return user1.points > user2.points
        }
       
        filterArray = dataArrays.sorted(by: { $0.points > $1.points})
         */
 
        //filter
        filterArray = dataArrays.filter({ (user) -> Bool in
            return user.points > 10
        })
        
        //map
        /*
//        mappedArray = dataArrays.map({ (user) -> String in
//            return user.name
//        })
        
        //mappedArray = dataArrays.map({$0.name})
        
//        mappedArray = dataArrays.compactMap({ (user) -> String? in
//            return user.name
//        })
        
        //mappedArray = dataArrays.compactMap({ $0.name})
    */
    }
    
    func getUsers() {
        let user1 = ArraysUserModel(name: "Rafi", points: 10, isVerified: true)
        let user2 = ArraysUserModel(name: "Sayem", points: 12, isVerified: false)
        let user3 = ArraysUserModel(name: "Sadid", points: 5, isVerified: true)
        let user4 = ArraysUserModel(name: "Sajib", points: 7, isVerified: false)
        let user5 = ArraysUserModel(name: "Tawfique", points: 4, isVerified: true)
        let user6 = ArraysUserModel(name: "Rana", points: 6, isVerified: false)
        let user7 = ArraysUserModel(name: "Shahin", points: 9, isVerified: true)
        let user8 = ArraysUserModel(name: "Laky", points: 8, isVerified: false)
        let user9 = ArraysUserModel(name: "Yousuf", points: 11, isVerified: true)
        let user10 = ArraysUserModel(name: "Shamim", points: 15, isVerified: false)
        let user11 = ArraysUserModel(name: "Yakub", points: 11, isVerified: true)
        
        self.dataArrays.append(contentsOf: [user1, user2, user3, user4, user5, user6, user7, user8, user9, user10, user11])
    }
}

struct ArraysBootCamp: View {
    
    @StateObject var vm = ArraysModificationViewMOdel()
    
    var body: some View {
        ScrollView {
            VStack (spacing: 10) {
                ForEach(vm.filterArray) { user in
                    VStack (alignment: .leading){
                        Text(user.name)
                            .font(.headline)
                        
                        Divider()
                        
                        HStack {
                            Text("Points: \(user.points)")
                            
                            Spacer()
                            if user.isVerified {
                                Image(systemName: "flame.fill")
                            }
                            
                        }
                    }
                    .foregroundColor(.white)
                    .padding()
                    .background(Color.blue.cornerRadius(10))
                    .padding(.horizontal)
                }
            }
        }
    }
}

struct ArraysBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        ArraysBootCamp()
    }
}
