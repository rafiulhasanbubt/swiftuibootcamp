//
//  CodableBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 5/5/21.
//

import SwiftUI

struct CustomerModel: Identifiable {
    let id: String
    let name: String
    let points: Int
    let isPremium: Bool
}

class CodableViewModel: ObservableObject {
    @Published var customer: CustomerModel? = CustomerModel(id: "we", name: "Rafi", points: 10, isPremium: true)
    
    
    func getData() {
        guard let data = getJSONData() else { return }
        print(data)
        let jsonString = String(data: data, encoding: .utf16)
        print(jsonString)
    }
    
    func getJSONData() -> Data? {
        
        let dictionary: [String: Any] = [
            "id": "1234",
            "name": "rafi",
            "points": 1234,
            "isPremium": true,
        ]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        return jsonData
    }
}

struct CodableBootCamp: View {
    @StateObject var vm = CodableViewModel()
    
    var body: some View {
        VStack {
            if let customer = vm.customer {
                Text(customer.id)
                Text(customer.name)
                Text("\(customer.points)")
                Text(customer.isPremium.description)
            }
        }
    }
}

struct CodableBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        CodableBootCamp()
    }
}
