//
//  WeakSelfBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 4/5/21.
//

import SwiftUI

struct WeakSelfBootCamp: View {
    @AppStorage("count") var count: Int?
    
    var body: some View {
        NavigationView {
            NavigationLink("Navigate", destination: WeakSelfSecondscreen())
                .navigationTitle("Screen 1")
        }
        .overlay(
            Text("\(count ??  0)")
                .font(.largeTitle)
                .padding()
                .background(Color.green.cornerRadius(10))
            , alignment: .topTrailing
        )
    }
}

struct WeakSelfSecondscreen: View {
    
    @StateObject var vm = WeakSelfSecondscreenViewModel()
    
    var body: some View {
        VStack {
            Text("Second View")
                .font(.largeTitle)
                .foregroundColor(.red)
            
            if let data = vm.data {
                Text(data)
            }
        }
    }
}


class WeakSelfSecondscreenViewModel: ObservableObject {
    @Published var data: String? = nil
    
    init() {
        print("Initialize")
        let currentCount = UserDefaults.standard.integer(forKey: "count")
        UserDefaults.standard.set(currentCount + 1, forKey: "count")
        
        getData()
    }
    
    deinit {
        print("Deinitialize")
        let currentCount = UserDefaults.standard.integer(forKey: "count")
        UserDefaults.standard.set(currentCount - 1, forKey: "count")
    }
    func getData() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 500 ) {
            self.data = "New Data!"
        }
        
//        DispatchQueue.global().async {
//            self.data = "New Data!"
//        }
    }
}


struct WeakSelfBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        WeakSelfBootCamp()
    }
}
