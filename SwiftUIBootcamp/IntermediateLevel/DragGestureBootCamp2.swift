//
//  DragGestureBootCamp2.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 30/4/21.
//

import SwiftUI

struct DragGestureBootCamp2: View {
    @State var startingOffsetY: CGFloat = UIScreen.main.bounds.height * 0.8
    @State var currentOffsetY: CGFloat = 0
    @State var endingOffsetY: CGFloat = 0
    
    var body: some View {
        ZStack {
            Color.green.ignoresSafeArea()
            
            MySignupView()
                .offset(y: startingOffsetY)
                .offset(y: currentOffsetY)
                .offset(y: endingOffsetY)
                .gesture(
                    DragGesture()
                        .onChanged { value in
                            withAnimation(.spring()) {
                                currentOffsetY = value.translation.height
                            }
                        }
                        .onEnded { value in
                            withAnimation(.spring()) {
                                if currentOffsetY < -150 {
                                    endingOffsetY = -startingOffsetY
                                    currentOffsetY = 0
                                } else if endingOffsetY != 0 && currentOffsetY > 150 {
                                    endingOffsetY = 0
                                    currentOffsetY = 0
                                } else {
                                    currentOffsetY = 0
                                }
                            }
                        }
                )
        }
        .ignoresSafeArea(edges: .bottom)
    }
}

struct DragGestureBootCamp2_Previews: PreviewProvider {
    static var previews: some View {
        DragGestureBootCamp2()
    }
}

struct MySignupView: View {
    var body: some View {
        VStack (spacing: 20) {
            Image(systemName: "chevron.up")
                .padding(.top)
            Text("Sign up")
                .font(.headline)
                .fontWeight(.semibold)
            Image(systemName: "flame.fill")
                .resizable()
                .scaledToFit()
                .frame(width: 100, height: 100)
            Text("This is the description for my app.")
            Text("Create an account")
                .foregroundColor(.white)
                .font(.headline)
                .padding()
                .padding(.horizontal)
                .background(Color.black.cornerRadius(10))
            
            Spacer()
        }
        .frame(maxWidth: .infinity)
        .background(Color.white)
        .cornerRadius(30)
    }
}
