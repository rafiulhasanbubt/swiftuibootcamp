//
//  ScrollViewReaderBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 1/5/21.
//

import SwiftUI

struct ScrollViewReaderBootCamp: View {
    @State var textFieldText: String = ""
    @State var scrollToIndex: Int = 0
    
    var body: some View {
        VStack {
            TextField("Enter a # here...", text: $textFieldText)
                .frame(height: 44)
                .border(Color.gray)
                .padding(.horizontal)
                .keyboardType(.numberPad)
            
            Button("Scroll now".uppercased()) {
                withAnimation(.spring()) {
                    if let index = Int(textFieldText) {
                        scrollToIndex = index
                    }
                }
            }
            
            ScrollView {
                ScrollViewReader { proxy in
//                    Button("Click here to go to # 30".uppercased()) {
//                        withAnimation(.spring()) {
//                            proxy.scrollTo(30, anchor: .center)
//                        }
//                    }
                    
                    ForEach(0 ..< 50) { item in
                        Text("This is item # \(item)")
                            .font(.headline)
                            .frame(height: 150)
                            .frame(maxWidth: .infinity)
                            .background(Color.white)
                            .cornerRadius(10)
                            .shadow(radius: 10)
                            .padding()
                            .id(item)
                    }.onChange(of: scrollToIndex, perform: { value in
                        proxy.scrollTo(value, anchor: .center)
                    })
                }
            }
        }
    }
}

struct ScrollViewReaderBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        ScrollViewReaderBootCamp()
    }
}
