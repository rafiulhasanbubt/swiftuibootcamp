//
//  LongPressGestureBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 30/4/21.
//

import SwiftUI

struct LongPressGestureBootCamp: View {
    @State var isComplete: Bool = false
    @State var isSuccess: Bool = false
    
    var body: some View {
        VStack {
            Spacer()
            VStack {
                Rectangle()
                    .fill(isSuccess ? Color.green : Color.blue)
                    .frame(maxWidth: isComplete ? .infinity : 0)
                    .frame(height: 44)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .background(Color.gray)
                HStack {
                    Text("Click here".uppercased())
                        .foregroundColor(.white)
                        .padding()
                        .background(Color.black)
                        .cornerRadius(10)
                        .onLongPressGesture(minimumDuration: 1, maximumDistance: 50) { (isPressing) in
                            //Start of press -> min duration
                            if isPressing {
                                withAnimation(.easeInOut(duration: 1)) {
                                    isComplete.toggle()
                                }
                            } else {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                    withAnimation(.easeInOut) {
                                        isSuccess = false
                                    }
                                }
                            }
                        } perform: {
                            //at the min duration
                            withAnimation(.easeInOut) {
                                isSuccess = true
                            }
                        }

                      
                    Text("reset".uppercased())
                        .foregroundColor(.white)
                        .padding()
                        .background(Color.black)
                        .cornerRadius(10)
                        .onTapGesture {
                            isComplete = false
                            isSuccess = false
                        }
                }
            }
            
            Spacer()
            
            VStack {
                Text(isComplete ? "Complete".uppercased() : "not complete".uppercased())
                    .padding()
                    .padding(.horizontal)
                    .background(isComplete ? Color.green : Color.gray)
                    .cornerRadius(10)
                    //.onTapGesture {isComplete.toggle()}
                    //.onLongPressGesture {isComplete.toggle()}
                    .onLongPressGesture(minimumDuration: 2, maximumDistance: 1) {isComplete.toggle()}
            }
            
            Spacer()
        }
    }
}

struct LongPressGestureBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        LongPressGestureBootCamp()
    }
}
