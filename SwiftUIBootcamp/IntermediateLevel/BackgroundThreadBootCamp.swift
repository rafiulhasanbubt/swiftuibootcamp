//
//  BackgroundThreadBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 3/5/21.
//

import SwiftUI

class BackgroundThreadViewModel: ObservableObject {
    @Published var dataArray: [String] = []
    
    func fetchData() {
        DispatchQueue.global().async {
            let newData = self.downloadData()
            DispatchQueue.main.async {
                self.dataArray = newData
                
            }
        }
    }
    
    private func downloadData() -> [String] {
        var data: [String] = []
        for x in 0..<100 {
            data.append("\(x)")
            print(data)
        }
        return data
    }
    
}

struct BackgroundThreadBootCamp: View {
    @StateObject var vm: BackgroundThreadViewModel = BackgroundThreadViewModel()
    
    var body: some View {
        ScrollView {
            VStack {
                Text("Load Data")
                    .font(.largeTitle)
                    .fontWeight(.semibold)
                    .onTapGesture {
                        vm.fetchData()
                    }
                
                ForEach(vm.dataArray, id: \.self) { item in
                    Text(item)
                        .font(.headline)
                        .foregroundColor(.red)
                        .frame(height: 44)
                        .frame(maxWidth: .infinity )
                        .background(Color.white)
                        .shadow(radius: 4)
                        .padding(.horizontal)
                }
            }
        }
    }
}

struct BackgroundThreadBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        BackgroundThreadBootCamp()
    }
}
