//
//  MultipleSheetsBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 1/5/21.
//

import SwiftUI

struct RandomModel: Identifiable {
    let id = UUID().uuidString
    let title: String
}

struct MultipleSheetsBootCamp: View {
    @State var selectedModel: RandomModel = RandomModel(title: "Starting title")
    @State var showSheet: Bool = false
    
    var body: some View {
        VStack (spacing: 20) {
            Button("Button 1") {
                selectedModel = RandomModel(title: "One")
                showSheet.toggle()
            }
            
            Button("Button 2") {
                selectedModel = RandomModel(title: "Two")
                showSheet.toggle()
            }
            .sheet(isPresented: $showSheet, content: {
                NextScreen(selectedModel: selectedModel)
            })
        }
    }
}

struct NextScreen: View {
    let selectedModel: RandomModel
    
    var body: some View {
        Text(selectedModel.title)
            .font(.largeTitle)
    }
}

struct MultipleSheetsBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        MultipleSheetsBootCamp()
    }
}
