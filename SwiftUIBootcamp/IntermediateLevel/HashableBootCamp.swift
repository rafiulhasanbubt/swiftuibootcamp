//
//  HashableBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 2/5/21.
//

import SwiftUI

struct MyCustomModel: Hashable {
    let title: String
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(title)
    }
}

struct HashableBootCamp: View {
    //let data: [String] = ["One", "Two", "Three", "Four", "Five"]
    let data: [MyCustomModel] = [
        MyCustomModel(title: "One"),
        MyCustomModel(title: "Two"),
        MyCustomModel(title: "Three"),
        MyCustomModel(title: "Four"),
        MyCustomModel(title: "five")
    ]
    
    var body: some View {
        ScrollView {
            VStack (spacing: 20) {
                ForEach(data, id: \.self) { item in
                    Text(item.title)
                        .font(.headline)
                }
            }
        }
    }
}

struct HashableBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        HashableBootCamp()
    }
}
