//
//  MagnificationGestureBootCamp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 30/4/21.
//

import SwiftUI

struct MagnificationGestureBootCamp: View {
    @State var currentAmount: CGFloat = 0
    @State var lastAmount: CGFloat = 0
    
    var body: some View {
        VStack {
            Spacer()
            //real life example instragram feed
            VStack (spacing: 10) {
                HStack {
                    Circle().frame(width: 35, height: 35)
                    Text("Rafiul Hasan")
                    Spacer()
                    Image(systemName: "ellipsis")
                }
                
                Rectangle()
                    .frame(height: 200)
                    .scaleEffect(1 + currentAmount)
                    .gesture(
                        MagnificationGesture()
                            .onChanged({ value in
                                currentAmount = value + 1
                            })
                            .onEnded({ value in
                                withAnimation(.spring()) {
                                    currentAmount = 0
                                }
                            })
                    )
                
                HStack {
                    Image(systemName: "heart.fill")
                    Image(systemName: "text.bubble.fill")
                    Spacer()
                }
                .padding(.horizontal)
                .font(.headline)
                
                Text("This is the caption for my photo!")
                    .frame(maxWidth: .infinity, alignment: .leading)
            }
            
            Spacer()
            
            VStack {
                Text("Hello, World!")
                    .font(.title)
                    .padding(20)
                    .padding(.horizontal)
                    .background(Color.red.cornerRadius(20))
                    .scaleEffect(1 + currentAmount + lastAmount)
                    .gesture(
                        MagnificationGesture()
                            .onChanged({ value in
                                currentAmount = value - 1
                            })
                            .onEnded({ value in
                                lastAmount = lastAmount + currentAmount
                            })
                )
            }
            
            Spacer()
        }
    }
}

struct MagnificationGestureBootCamp_Previews: PreviewProvider {
    static var previews: some View {
        MagnificationGestureBootCamp()
    }
}
