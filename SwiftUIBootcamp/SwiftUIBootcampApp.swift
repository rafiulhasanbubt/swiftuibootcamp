//
//  SwiftUIBootcampApp.swift
//  SwiftUIBootcamp
//
//  Created by Alesha Tech on 26/4/21.
//

import SwiftUI

@main
struct SwiftUIBootcampApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
